module gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service

go 1.13

require (
	github.com/99designs/gqlgen v0.10.1
	github.com/danielkov/gin-helmet v0.0.0-20171108135313-1387e224435e
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/gorilla/websocket v1.4.1
	github.com/jinzhu/gorm v1.9.11
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
	github.com/ugorji/go v1.1.7 // indirect
	github.com/vektah/gqlparser v1.1.2
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
