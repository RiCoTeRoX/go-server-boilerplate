FROM golang:1.13.1-alpine3.10 as builder

WORKDIR /opt/drawingfy-game-service/build
ADD . /opt/drawingfy-game-service/build/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .
FROM scratch
COPY --from=builder /opt/drawingfy-game-service/build/main /app/
WORKDIR /app
ARG HOST
ENV HOST ${HOST:-localhost}
ARG PORT
ENV PORT ${PORT:-3000}
ARG LOG_LEVEL
ENV LOG_LEVEL ${LOG_LEVEL:-INFO}
CMD ["./main"]
