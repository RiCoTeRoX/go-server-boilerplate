package main

import (
	"fmt"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/server"
	
	"os"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})
	
	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)
	
	// Only log the warning severity or above.
	log.SetLevel(log.WarnLevel)
	
	
	if err := godotenv.Load(); err != nil {
		fmt.Println("Config load error", err.Error())
	}
}

func main() {
	logLevel, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		panic(err)
	}
	log.SetLevel(logLevel)
	server.Start()
}
