package server

import (
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/router"
	"os"
)

//type Product struct {
//	gorm.Model
//	Code  string
//	Price uint
//}

func Start() {
	
	//username := os.Getenv("db_user")
	//password := os.Getenv("db_pass")
	//dbName := os.Getenv("db_name")
	//dbHost := os.Getenv("db_host")
	//username := "postgres"
	//password := "postgres"
	//dbName := "postgres"
	//dbHost := "0.0.0.0"
	//port := "5432"
	
	//dbUri := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, port, username, dbName, password) //Build connection string
	
	//db, err := gorm.Open("postgres", dbUri)
	//fmt.Println(dbUri)
	//if err != nil {
	//	fmt.Println(err.Error())
	//	panic("failed to connect database")
	//}
	//defer db.Close()
	//
	//// Migrate the schema
	//db.AutoMigrate(&Product{})
	//
	//// Create
	//db.Create(&Product{Code: "L1212", Price: 1000})
	//
	//// Read
	//var product Product
	//db.First(&product, 1) // find product with id 1
	//db.First(&product, "code = ?", "L1212") // find product with code l1212
	//
	//// Update - update product's price to 2000
	//db.Model(&product).Update("Price", 2000)
	//
	//// Delete - delete product
	//db.Delete(&product)
	
	
	
	// config := AppConfig.GetConfig()
	
	r := router.SetRouter()
	
	r.LoadHTMLFiles("src/html/index.html")
	
	r.GET("/index2", func(c *gin.Context) {
		c.HTML(200, "index.html", nil)
	})

	
	_ = r.Run(os.Getenv("HOST") + os.Getenv("PORT"))
}


