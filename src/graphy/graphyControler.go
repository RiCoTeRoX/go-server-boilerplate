package graphy

import (
	"github.com/99designs/gqlgen/handler"
	"github.com/gin-gonic/gin"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/graphy/auto"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/graphy/resolvers"
)

func Register(graphyGroup string,r *gin.Engine) {
	userRoute := r.Group(graphyGroup)
	userRoute.GET("/", handleHome)
	userRoute.POST("/query", handleQuery)
}

func handleQuery(c *gin.Context)  {
	
	h := handler.GraphQL(auto.NewExecutableSchema(auto.Config{Resolvers: &resolvers.Resolver{}}))
	h.ServeHTTP(c.Writer, c.Request)
}

func handleHome(c *gin.Context) {
	h := handler.Playground("GraphQL playground", "/graph/query")
	h.ServeHTTP(c.Writer, c.Request)
}
