package ws

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"net/http"
)

func Register(group string, r *gin.Engine) {
	userRoute := r.Group(group)
	userRoute.GET("", defaultHandler)
}

func defaultHandler(c *gin.Context) {
	wshandler(c.Writer, c.Request)
}

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}
	
	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		conn.WriteMessage(t, msg)
	}
}

