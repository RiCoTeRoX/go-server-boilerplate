package config

import (
	"fmt"
	"github.com/spf13/viper"
)

var config *viper.Viper

func init() {
	
	
	
	env := "develop"
	viper := viper.New()
	viper.SetConfigName(env)
	viper.AddConfigPath("src/config")
	viper.SetConfigType("toml")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	} else {
		devServer := viper.GetString("develop.server")
		devConnectionMax := viper.GetInt("develop.connection_max")
		devEnabled := viper.GetBool("develop.enabled")
		devPort := viper.GetString("develop.port")

		fmt.Printf("\nDevelopment Config found:\n server = %s\n connection_max = %d\n"+
			" enabled = %t\n"+
			" port = %s\n",
			devServer,
			devConnectionMax,
			devEnabled,
			devPort)
	}
	viper.AutomaticEnv()
	config = viper
}

func GetConfig() *viper.Viper {
	return config
}
