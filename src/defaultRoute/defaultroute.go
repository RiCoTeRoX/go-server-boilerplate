package defaultRoute


import (
"github.com/gin-gonic/gin"
"net/http"
)

func Register(group string, r *gin.Engine) {
	userRoute := r.Group(group)
	userRoute.GET("/", defaultHandler)
}

func defaultHandler(c *gin.Context) {
	c.String(http.StatusOK, "Hello router")
}
