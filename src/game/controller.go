package game

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Register(group string, r *gin.Engine) {
	userRoute := r.Group(group)
	userRoute.GET("/", handleHome)
	userRoute.GET("/:id", handleGame)
}

func handleGame(c *gin.Context)  {
	c.String(http.StatusOK, "sos el " + c.Param("id"))
}

func handleHome(c *gin.Context) {
	c.String(http.StatusOK, "Hello game")
}
