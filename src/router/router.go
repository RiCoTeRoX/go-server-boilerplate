package router

import (
	helmet "github.com/danielkov/gin-helmet"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/defaultRoute"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/game"
	"gitlab.com/RiCoTeRoX/drawingfy-microservices/game-service/src/ws"
	"time"
)

func SetRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	//router := gin.Default()
	r := gin.Default()
	r.Use(helmet.Default())
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge: 12 * time.Hour,
	}))
	
	
	defaultRoute.Register("/", r)
	game.Register("/game", r)
	ws.Register("/ws", r)
	
	//router.Use(CorsMiddleware())
	
	//router.Use(CheckLogin())
	// load the casbin model and policy from files, database is also supported.
	//e := casbin.NewEnforcer("conf/authz_model.conf", "conf/authz_policy.csv")
	//router.Use(authz.NewAuthorizer(e))
	
	//cookie session
	//store := cookie.NewStore([]byte("secret"))
	//router.Use(sessions.Sessions("mysession", store))
	
	//redis session
	//store, _ := redis.NewStore(10, "tcp", "localhost:6379", "", []byte("secret"))
	//router.Use(sessions.Sessions("mysession", store))
	
	// Ping test
	
	//router.GET("/ping", func(c *gin.Context) {
	//	c.String(http.StatusOK, "pong")
	//})
	//
	//router.GET("/basic/basic", basic.GetBasicInfo)
	//
	//router.NoRoute(func(c *gin.Context) {
	//	c.JSON(http.StatusNotFound, gin.H{
	//		"status": 404,
	//		"msg":    "接口不存在->('.')/请求方法不存在",
	//	})
	//})
	
	return r
}

//func CheckLogin() gin.HandlerFunc {
//	return func(c *gin.Context) {
//		path := c.Request.URL.String()
//		if !strings.Contains(path, "login") && !strings.Contains(path, "/static/file") {
//			_, err := c.Cookie("uid")
//			if err != nil {
//				c.Abort()
//				c.JSON(http.StatusOK, lib.MapNoAuth)
//			}
//		}
//	}
//}

///*跨域解决方案,待完善,建议nginx 解决*/
//func CorsMiddleware() gin.HandlerFunc {
//	return func(c *gin.Context) {
//		method := c.Request.Method
//		origin := c.Request.Header.Get("Origin")
//		var filterHost = [...]string{"http://localhost.*", "http://192.168.31.173:3000"}
//		// filterHost 做过滤器，防止不合法的域名访问
//		var isAccess = false
//		for _, v := range filterHost {
//			match, _ := regexp.MatchString(v, origin)
//			if match {
//				isAccess = true
//			}
//		}
//		if isAccess {
//			// 核心处理方式
//			c.Header("Access-Control-Allow-Origin", "*")
//			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
//			c.Header("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE")
//			c.Set("content-type", "application/json")
//		}
//		//放行所有OPTIONS方法
//		if method == "OPTIONS" {
//			c.JSON(http.StatusOK, "Options Request!")
//		}
//		c.Next()
//	}
//}

